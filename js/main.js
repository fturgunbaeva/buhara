var swiper = new Swiper('.slide-banner', {
    loop: true,
    autoplay: true,
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },
});

var swiper = new Swiper('.slide-restaurant', {
    loop: true,
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },
});

var swiper = new Swiper('.reviews', {
    slidesPerView: 3,
    spaceBetween: 30,
    loop: true,
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },
});
var swiper = new Swiper('.popular', {
    slidesPerView: 4.5,
    spaceBetween: 32,
    loop: true,
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },
});
var swiper = new Swiper('#restaurant-slider', {
    slidesPerView: 3,
    spaceBetween: 24,
    loop: true,
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },
});





var maxLength = 324;
$('#inputChars').keyup(function() {
    var lengthChars = $(this).val().length;
    //var length = maxLength-length;
    $('#chars').text(lengthChars);
    if(lengthChars > 1){
       $('.greyBtn').addClass('active');
    } else {
        $('.greyBtn').removeClass('active')
    }
});

$(".like").click(function(){
    if ($(this).hasClass("active")){
        $(this).removeClass("active");
    }
    else {
        $(this).addClass("active");
    }
});

$('.item').find('img').click(function () {
    if($(".item-modal").hasClass('actie')){
        $(".item-modal").removeClass('active')
    } else {
        $(".item-modal").addClass("active");
    }
    console.log('ok')
});

$(".cartModalClose").click(function(){
    $('.item-modal').removeClass("active");
});
$(document).ready(function () {
    toggleActiveClass();
});
function toggleActiveClass() {
    let boxes = $('.dish-item');
    $(boxes).each(function(i, obj) {
        var deleteIcon = $(obj).find('.delete-icon');
        deleteIcon.unbind('click').bind('click', function(e) {
            e.preventDefault();
            setActive(i);
        });
        var cancel = $(obj).find('.cancel');
        cancel.unbind('click').bind('click', function(e) {
            e.preventDefault();
            setActiveCancel(i);
        });
    });
    var setActive = function (index) {
        $(boxes).each(function(i, obj) {
            var modal = $(obj).find('.delete-modal');
            if (i === index) {
                modal.toggle()
            }
        });
    };
    var setActiveCancel = function (index) {
        $(boxes).each(function(i, obj) {
            var modal = $(obj).find('.delete-modal');
            if (i === index) {
                modal.hide();
            }
        });
    }
}

$( ".target-drop-box" ).click(function() {
    $( this ).toggleClass( "show-box" );
});
$( ".search" ).click(function() {
    $(".search-dropdown").toggleClass( "search-show-box" );


    // if($( this ).hasClass( "search-show-box" ) ){
    //     $( this ).removeClass( "search-show-box" );
    // } else {
    //     $( this ).addClass( "search-show-box" );
    // }
});
$(".target-drop-basket").click(function () {
    $(".basket").toggleClass('active')
    $(".drop-menu-basket").toggleClass( "basket-show-box" );
});

$('.select').on('click','.placeholder',function(){
    var parent = $(this).closest('.select');
    if ( ! parent.hasClass('is-open')){
        parent.addClass('is-open');
        $('.select.is-open').not(parent).removeClass('is-open');
    }else{
        parent.removeClass('is-open');
    }
}).on('click','ul>li',function(){
    var parent = $(this).closest('.select');
    parent.removeClass('is-open').find('.placeholder').text( $(this).text() );
    parent.find('input[type=hidden]').attr('value', $(this).attr('data-value') );
    console.log()
});

//count function(plus, minus for basket)

$('.count').find('.plus').click(function () {
    if ($(this).prev().val() < 30) {
        $(this).prev().val(+$(this).prev().val() + 1);
    }
});


$('.count').find('.minus').click(function () {
    if ($(this).next().val() > 1) {
        if ($(this).next().val() > 1) $(this).next().val(+$(this).next().val() - 1);
    }
});


$(".paymentBtn").click(function(){
    $(".paymentModal").addClass("active");
});

$(".paymentModal").find('.paymentModalClose').click(function () {
    $(".paymentModal").removeClass( "active" );
});


$( ".signIn" ).click(function() {
    if ( $(".enter-form").hasClass( "active" )) {
        $(".enter-form").removeClass( "active" );
        $(".registration").toggleClass( "active" );
    } else {
        $(".registration").toggleClass( "active" );
    }
    $(".enter-code").removeClass( "active" );
});

$( ".logIn" ).click(function() {
    if ($(".registration").hasClass( "active" )) {
        $(".registration").removeClass( "active" );
        $(".enter-form").toggleClass( "active" );
    } else {
        $(".enter-form").toggleClass( "active" );
    }
    $(".enter-code").removeClass( "active" );
});
$(".signInNext").click(function () {
    $(".enter-code").toggleClass( "active" );

});

$(".signInCodeSend").click(function () {
    $(".enter-code").removeClass( "active" );
    $(".enter-form").removeClass( "active" );

});

$(".user").click(function () {
    if ($(".drop-out").hasClass('active')) {
        $(".drop-out").removeClass('active')
        $(".user-drop-menu").toggleClass( "active" )} else {
    $(".user-drop-menu").toggleClass( "active" )};
});

$(".out").click(function () {
    // if ($(".drop-out").hasClass('active')) {
    //     $(".drop-out").removeClass('active')
    // } else {
        $(".drop-out").addClass('active')
    // }
});


$(".drop-signOut").click(function () {
    $(".user-drop-menu").removeClass( "active" );
    $(".drop-out").removeClass('active')
});